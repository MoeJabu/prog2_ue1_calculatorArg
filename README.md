# CalculatorArg
The program provides the basic functionality of a calculator. These are addition, subtraction, multiplication and division.  

The program is designed to be started from the command line with three parameters.
All of them are mandatory.  

#### Format is:  
_CalculatorArg NUMBER1 OPERATOR NUMBER2_

#### Example
Input:
``` bash
$ java CalculatorArg 5 + 7
```
Output:
```
5.00 + 7.00 = 12.00
```
### Possible Errors and Exit-Codes
If there are multiple errors in the input the one with the lowest number will be throwen.

| Exit-code | Problem                      | Console Error  |
|----|----|----|
| 1 | an uncorrect number of parameters, was provided | Error - program must have 3 argumentes|
| 2 | the provided operator-symbol is unknown. the operator must be one of these symbols: +, -, *, / | Error - arguments must be of type NUMBER1 {'*'|'/'|'+'|'-'} NUMBER2 |
| 3 | one of the provided numbers is in a non accepted format. The program acceptes integers like 1, 24 or 5000 and floating point numbers formated with a dot like 12.6, 587.24, 56.10 . | Error - one of the provided numbers, is wrongly formatted|

___

# CalculatorSys

The program provides the basic functionality of a calculator. These are addition, subtraction, multiplication and division.

The program will read user input during run time. In case of an uncorrect input, the user will be reasked immediately by the program.

### possible Input

- __Value 1__ and __Value 2__ must be an integer or a floating point number delimited by a dot, like 14.30. Using white spaces or any other symbol before or after the number will lead to an error.  
- __Operator__ must be one of these symbols: +, -, *, /.  

#### Example
``` bash
$ java CalculatorSys
        Value 1: 14.30
        Operator: +
        Value 2: 57
```
```
14.30 + 57.00 = 71.30
```
