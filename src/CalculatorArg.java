/*CaltulatorArg
 * Provides basic calculator functions to handle comandoline argumentes
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 9.3.2021
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class CalculatorArg {

	public static void main(String[] args) {

		Locale.setDefault(new java.util.Locale("en","US"));
		
		
		if (args.length == 3) {

			List<String> validOperators = new ArrayList<String>(List.of("+", "-", "*", "/"));

			String firstNumberStr = args[0].trim();
			String operator = args[1].trim();
			String secondNumberStr = args[2].trim();

			if (validOperators.contains(operator)) {
				Double result = 0.0;
				Double firstNumber = 0.0;
				Double secondNumber = 0.0;
				
				try {
					firstNumber = Double.parseDouble(firstNumberStr);
					secondNumber = Double.parseDouble(secondNumberStr);
				}
				
				catch(NumberFormatException e) {
					System.out.println("Error - one of the provided numbers, is wrongly formatted");
					System.exit(3);
				}

				switch (operator) {
				case "+":
					result = firstNumber + secondNumber;
					break;
				case "-":
					result = firstNumber - secondNumber;
					break;
				case "*":
					result = firstNumber * secondNumber;
					break;
				case "/":
					result = firstNumber / secondNumber;
					break;
				}
				
				System.out.printf("%.2f %s %.2f = %.2f", firstNumber, operator, secondNumber, result);
			} else {
				System.out.println("Error - arguments must be of type NUMBER1 {'*'|'/'|'+'|'-'} NUMBER2");
				System.exit(2);
			}

		} else {
			System.out.println("Error - program must have 3 argumentes");
			System.exit(1);
		}
	}

}
